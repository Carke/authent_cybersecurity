import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services/user.service";
import {first} from "rxjs/operators";
import {User} from "../../models/User.model";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  loading = false;
  users?: User[];

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
    this.loading = true;
    this.userService.getAll().pipe(first()).subscribe(users => {
      this.loading = false;
      if(users) {this.users = users;}
    });
  }

}
