import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmeliFormComponent } from './ameli-form.component';

describe('AmeliFormComponent', () => {
  let component: AmeliFormComponent;
  let fixture: ComponentFixture<AmeliFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmeliFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmeliFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
