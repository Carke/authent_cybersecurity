import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MissingRequiredValues} from "../../errors/missing-required-values";
import {MatIconRegistry} from "@angular/material/icon";
import {DomSanitizer} from "@angular/platform-browser";
import {AuthService} from "../../services/auth-service.service";
import {ActivatedRoute, Router} from "@angular/router";
import {first} from "rxjs/operators";

@Component({
  selector: 'app-ameli-form',
  templateUrl: './ameli-form.component.html',
  styleUrls: ['./ameli-form.component.scss']
})
export class AmeliFormComponent implements OnInit {

  loading = false;
  submitted = false;
  returnUrl?: string;
  error = '';
  hide: boolean = true;
  matcher = new MissingRequiredValues();
  authenticationForm: FormGroup = this.fb.group({
    j_username: [null, [Validators.required,]],
    j_password: [null, [Validators.required,]],
    j_etape: 'CLASSIQUE'
  });

  constructor(
    private fb: FormBuilder,
    private matIconLoginRegistry: MatIconRegistry,
    private matIconPwdRegistry: MatIconRegistry,
    private matIconWarnRegistry: MatIconRegistry,
    private matIconStrikedEyeRegistry: MatIconRegistry,
    private matIconEyeRegistry: MatIconRegistry,
    private matIconCloseRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthService,
  ) {
    this.matIconLoginRegistry.addSvgIcon(
      "login",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../assets/img/login.svg")
    );
    this.matIconPwdRegistry.addSvgIcon(
      "password",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../assets/img/password.svg")
    );
    this.matIconWarnRegistry.addSvgIcon(
      "warning",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../assets/img/warning_30px.svg")
    );
    this.matIconStrikedEyeRegistry.addSvgIcon(
      "hide",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../assets/img/OeilBarre.svg")
    );
    this.matIconEyeRegistry.addSvgIcon(
      "visibility",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../assets/img/Oeil.svg")
    );
    this.matIconCloseRegistry.addSvgIcon(
      "close",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../assets/img/close_20px.svg")
    );
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit(): void
  {
    this.authenticationForm = this.fb.group({
      j_username: [null, [Validators.required,]],
      j_password: [null, [Validators.required,]]
    });
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.authenticationForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.authenticationForm.invalid) {
      console.log("invalid form !")
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }

}
