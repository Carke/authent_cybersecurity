import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AmeliFormComponent} from "./components/ameli-form/ameli-form.component";
import {UserListComponent} from "./components/user-list/user-list.component";
import {AuthGuardService} from "./services/auth-guard.service";

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: AmeliFormComponent },
  { path: 'user-list', component: UserListComponent, canActivate: [AuthGuardService] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
