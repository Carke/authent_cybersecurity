export class User {
  readonly id: number;
  username: number;
  password: number;
  readonly token?: string;

  constructor(
    id:number,
    username:number,
    password:number,
    token?: string
  ){
    this.id = id;
    this.username = username;
    this.password = password;
    this.token = token;
  }
}
