# FRANCE CONNECT - Ameli

## Sommaire
- Introduction
- Installation
- Notes

## Introduction

### Equipe

- Alexis MUSSET, B3DEV, CA Angers / CNP Assurance.
- Kevin CARRILLO, B3DEV, CA Angers / SESAM-Vitale.

### Objectif du projet

Reproduire un service d'authentification et pouvoir en récupérer les identifiants et mots de passe hashés des utilisateurs. Cet exervice se place dans le contexte d'un cours de cybersécurité.

### Technologies, outils et méthodes.

Nous avons travaillé en cycle en V, avec les outils Git, GitLab, Postman, PyCharm et IntelliJ.

Nous avons optés pour les technologies suivantes:
- Django - Framework Python (Backend);
- Angular - Framework JavaScript (Frontend);
- PostgreSQL - Base de données relationnelle.

## Installation

1) Installer Python 3.6+, Postgresql 12+ et Node.js.
2) Créer la base de données fc_ameli dans postgresql.
3) Cloner les dossiers du backend et du frontend.
4) Générer un environnement virtuel Python et le lancer.
5) En ligne de commande, se placer dans le dossier de backend, au niveau du fichier requirements.txt et saisir:

         python -m pip install --upgrade pip
         pip install requirements.txt

6) A la fin de l'installation, créer un fichier .env au niveau du fichier settings.py avec les informations suivantes:

        SK_DJANGO=django-insecure-nmnoh!@ed!8=d+sjaq857)q3&57q3xj6yb*sl-@#(e3gbl2hv3
        LOC=127.0.0.1
        LOC2=localhost
        DB_NAME=fc_ameli
        DB_USER=nom_admin_de_la_bdd
        DB_PASS=mdp_admin_de_la_bdd
        DB_HOSTING=IP_de_la_BDD
        DB_PORT=Port_de_la_BDD
        DB_ENGINE=django.db.backends.postgresql

7) En ligne de commande, se rendre au niveau du fichier manage.py et saisir:

        python manage.py migrate
        python manage.py runserver
        
7) Dans une nouvelle console, se rendre dans le dossier de frontend au niveau du fichier angular.json et saisir:

        npm i --no-fund
        ng serve
        
8) Enfin, rendez-vous à l'url: 
        
        localhost:4200/

## Notes

### Fonctionne
 
- Génération du token JWT lorsqu'un utilisateur connu se connecte.
- Mise en forme du formulaire identique à celui de france connect pour ameli:
        
    https://fc.assure.ameli.fr/FRCO-app/login

- Récupération des utilisateurs et de leur mot de passe hashé sur :

    http://127.0.0.1:8000/user-list

- Page de création d'utilisateurs:

    http://127.0.0.1:8000/admin/
    
### A faire

- Lien entre frontend et backend: actuellement, incident sur le bouton d'envoi du formulaire (Revoir RxJS).