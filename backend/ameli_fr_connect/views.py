from django.contrib.auth import get_user_model
from django.shortcuts import render
from rest_framework import generics
from ameli_fr_connect.serializers import UserSerializer
from django.contrib.auth.models import User


class UserList(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
