from django.apps import AppConfig


class AmeliFrConnectConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ameli_fr_connect'
