"""back_france_connect URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token
from ameli_fr_connect import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path(r'api-token-auth/', obtain_auth_token, name='api_token_auth'),
    # path(r'api-token-refresh/', refresh_jwt_token),
    path(r'user-list/', views.UserList.as_view(), name='user-list'),
    path(r'user-list/<int:pk>/', views.UserDetail.as_view(), name='user-detail'),
]
